import pickle
import typing

import pika
from pika.credentials import PlainCredentials


class ExchangeConnection:
    def __init__(self, host: str, port: int, credentials: PlainCredentials):
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host,
                                      port=port,
                                      credentials=credentials
                                      )
        )

        self._channel = self._connection.channel()

    def close_connection(self):
        self._connection.close()

    def declare_queue(self, queue_name: str):
        """
        Declare queue.

        :param queue_name: queue name to declare
        """
        self._channel.queue_declare(queue=queue_name)

    def set_consumer(self, callback_function: typing.Callable, queue: str, no_ack: bool = False):
        """
        Set consumer's callback function.

        :param callback_function: function to call to consume
        :param queue: name of the queue
        :param no_ack: do not use ack
        """
        self._channel.basic_consume(
            callback_function,
            queue=queue,
            no_ack=no_ack
        )

    def publish(self, message: object, queue: str):
        """
        Publish some not yet serialized object.

        :param message: object to serialize and publish
        :param queue: name of the queue
        """
        self._channel.basic_publish(exchange='',
                                    routing_key=queue,
                                    body=pickle.dumps(message)
                                    )

    def ack(self, delivery_tag):
        """
        Acknowledge the message.
        :param delivery_tag: use method.delivery_tag from callback
        """
        self._channel.basic_ack(delivery_tag)

    def start_consuming(self):
        """
        Start consuming messages.
        """
        self._channel.start_consuming()
