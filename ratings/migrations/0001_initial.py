# Generated by Django 2.0.5 on 2018-06-12 06:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('competitions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('update_time', models.DateTimeField()),
                ('section', models.IntegerField(default=0)),
                ('tg_id', models.IntegerField(default=0, null=True)),
                ('photo_id', models.CharField(max_length=100)),
                ('competition', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='competitions.Competition')),
            ],
            options={
                'verbose_name': 'Таблица рейтингов',
                'verbose_name_plural': 'Таблицы рейтингов',
                'db_table': 'table_screenshots',
            },
        ),
    ]
