from django.db import models


class Rating(models.Model):
    class Meta:
        db_table = "table_screenshots"
        verbose_name = 'Таблица рейтингов'
        verbose_name_plural = 'Таблицы рейтингов'

    update_time = models.DateTimeField(
        null=False,
    )

    section = models.IntegerField(
        null=False,
        default=0
    )

    competition = models.ForeignKey(
        "competitions.Competition",
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None
    )

    tg_id = models.IntegerField(
        null=True,
        default=0
    )

    photo_id = models.CharField(
        max_length=100,
        null=False,
    )
