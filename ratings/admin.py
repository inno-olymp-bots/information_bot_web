from django.contrib import admin

from .models import Rating


class UserAdmin(admin.ModelAdmin):
    readonly_fields = ('section', 'competition', 'tg_id', 'photo_id', 'update_time')


admin.site.register(Rating, UserAdmin)
