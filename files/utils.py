from core.file_like import FileLike


def read_file_field(f_f, name):
    if f_f:
        with f_f.open('rb') as f:
            return FileLike(bytes(f.read()), name)
    return None
