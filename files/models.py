from django.db import models
from django.db.models import Q
from pika.credentials import PlainCredentials

from exchange import ExchangeConnection
from files.texts import *
from information_bot_web import settings as s
from .utils import read_file_field


class PdfFields(models.Model):
    class Meta:
        verbose_name = 'Файлы'
        verbose_name_plural = 'Файлы'

    city_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Схема города',
    )

    city_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Схема города (текст)',
    )

    city_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    city_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    event_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Схема площадки',
    )

    event_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Схема площадки (текст)',
    )

    event_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    event_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    meal_1_day_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Питание 1 день',
    )

    meal_1_day_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание 1 день (текст)',
    )

    meal_1_day_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    meal_1_day_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    meal_2_day_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Питание 2 день',
    )

    meal_2_day_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание 2 день (текст)',
    )

    meal_2_day_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    meal_2_day_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    meal_3_day_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Питание 3 день',
    )

    meal_3_day_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание 3 день (текст)',
    )

    meal_3_day_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    meal_3_day_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    housing_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Проживание',
    )

    housing_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Проживание (текст)',
    )

    housing_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    housing_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    olymp_schedule_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Расписание олимпиады',
    )

    olymp_schedule_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Расписание олимпиады (текст)',
    )

    olymp_schedule_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    olymp_schedule_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    master_schedule_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Расписание мастер-классов',
    )

    master_schedule_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Расписание мастер-классов (текст)',
    )

    master_schedule_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    master_schedule_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    activity_schedule_pdf = models.FileField(
        null=True,
        blank=True,
        verbose_name='Расписание активности',
    )

    activity_schedule_text = models.TextField(
        null=True,
        blank=True,
        verbose_name='Расписание активности (текст)',
    )

    activity_schedule_id = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        editable=False,
    )

    activity_schedule_is_pct = models.BooleanField(
        editable=False,
        blank=True,
        default=False,
    )

    contacts = models.TextField(
        null=True,
        blank=True,
        verbose_name='Контакты ответственных за заселение',
    )

    attache = models.TextField(
        null=True,
        blank=True,
        verbose_name='Контакты атташе',
    )

    receiver = models.ForeignKey(
        "users.User",
        on_delete=models.SET_DEFAULT,
        verbose_name='Админ для получения pdf',
        default=None,
        limit_choices_to=~Q(tg_id=None),
        null=True
    )

    def save(self, *args, **kwargs):
        super(PdfFields, self).save(*args, **kwargs)

        city = read_file_field(self.city_pdf, CITY_FILE_NAME)
        event = read_file_field(self.event_pdf, EVENT_FILE_NAME)
        meal_1_day = read_file_field(self.meal_1_day_pdf, MEAL_1_FILE_NAME)
        meal_2_day = read_file_field(self.meal_2_day_pdf, MEAL_2_FILE_NAME)
        meal_3_day = read_file_field(self.meal_3_day_pdf, MEAL_3_FILE_NAME)
        housing = read_file_field(self.housing_pdf, HOUSING_FILE_NAME)
        olymp_schedule = read_file_field(self.olymp_schedule_pdf, OLYMP_FILE_NAME)
        master_schedule = read_file_field(self.master_schedule_pdf, MASTER_FILE_NAME)
        activity_schedule = read_file_field(self.activity_schedule_pdf, ACTIVITY_FILE_NAME)

        def is_pct(file):
            return True if file and file[:4] != b'%PDF' else False

        self.city_is_pct = is_pct(city)
        self.event_is_pct = is_pct(event)
        self.meal_1_day_is_pct = is_pct(meal_1_day)
        self.meal_2_day_is_pct = is_pct(meal_2_day)
        self.meal_3_day_is_pct = is_pct(meal_3_day)
        self.housing_is_pct = is_pct(housing)
        self.olymp_schedule_is_pct = is_pct(olymp_schedule)
        self.master_schedule_is_pct = is_pct(master_schedule)
        self.activity_schedule_is_pct = is_pct(activity_schedule)

        super(PdfFields, self).save(*args, **kwargs)

        message = [[city, "city_id"],
                   [event, "event_id"],
                   [meal_1_day, "meal_1_day_id"],
                   [meal_2_day, "meal_2_day_id"],
                   [meal_3_day, "meal_3_day_id"],
                   [housing, "housing_id"],
                   [olymp_schedule, "olymp_schedule_id"],
                   [master_schedule, "master_schedule_id"],
                   [activity_schedule, "activity_schedule_id"],
                   ]

        exchange = ExchangeConnection(
            s.RABBITMQ_HOST,
            s.RABBITMQ_PORT,
            PlainCredentials(s.RABBITMQ_USER, s.RABBITMQ_PASS)
        )

        exchange.declare_queue('id_finder')
        exchange.publish(
            message,
            'id_finder'
        )
        exchange.close_connection()

    def __str__(self):
        return "Файлы"
