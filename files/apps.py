from django.apps import AppConfig


class PDFConfig(AppConfig):
    name = 'files'
    verbose_name = 'Файлы'
