from django.contrib import admin
from files.models import PdfFields


class PDFAdmin(admin.ModelAdmin):
    readonly_fields = ()


admin.site.register(PdfFields, PDFAdmin)
