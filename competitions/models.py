from django.db import models
from django.contrib.postgres.fields import ArrayField


class CompetitionCategory(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name='название категории соревнований'
    )

    class Meta:
        verbose_name = 'Категория соревнований'
        verbose_name_plural = 'Категории соревнований'
        db_table = 'competition_categories'

    def __str__(self):
        return "{}".format(self.name)


class Competition(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='название соревнования',
    )

    category = models.ForeignKey(
        'competitions.CompetitionCategory',
        verbose_name='Категория',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True,
    )

    meal = models.ForeignKey(
        'info.Meal',
        verbose_name='Питание',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True,
        blank=True
    )

    link = models.CharField(
        max_length=512,
        verbose_name='ссылка на Google-таблицу',
    )

    fst_round_active = models.BooleanField(
        default=False,
        verbose_name='активировать',
    )

    fst_round_name = models.CharField(
        max_length=30,
        default='Первый тур',
    )

    fst_round_parser = models.ForeignKey(
        'Parser',
        verbose_name='Парсер',
        related_name='fst_round_parser',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True
    )

    snd_round_active = models.BooleanField(
        default=False,
        verbose_name='активировать',
    )

    snd_round_name = models.CharField(
        max_length=30,
        default='Второй тур',
    )

    snd_round_parser = models.ForeignKey(
        'Parser',
        verbose_name='Парсер',
        related_name='snd_round_parser',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True
    )

    winners_active = models.BooleanField(
        default=False,
        verbose_name='активировать',
    )

    winners_name = models.CharField(
        max_length=30,
        default='Победители',
    )

    winners_parser = models.ForeignKey(
        'Parser',
        verbose_name='Парсер',
        related_name='winner_parser',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True
    )

    def user_action(self, obj):
        pass

    class Meta:
        verbose_name = 'Соревнование'
        verbose_name_plural = 'Соревнования'
        db_table = 'competitions'

    def __str__(self):
        return "{}".format(self.name)


class Parser(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Название парсера',
    )

    list_name = models.CharField(
        max_length=50,
        verbose_name='Название листа'
    )

    range = models.CharField(
        max_length=30,
        verbose_name='Область значений'
    )

    columns = ArrayField(
        models.CharField(
            max_length=30,
        ),
        verbose_name='Названия колонок',
        size=10
    )

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name = 'Парсер'
        verbose_name_plural = 'Парсеры'
        db_table = 'parsers'
