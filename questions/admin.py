from django.contrib import admin
from questions.models import Question


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('user', 'question', 'sent',)
    readonly_fields = ('question', 'user', 'sent',)
    list_filter = ('sent', )

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return self.readonly_fields + ('answer',)
        return self.readonly_fields


admin.site.register(Question, QuestionAdmin)
