FROM innoolympbots/info_web

RUN mkdir /src/
RUN chmod 777 -R /src
WORKDIR /src/
RUN mkdir media
RUN chmod 777 media
COPY --chown=1000 . /src/

EXPOSE 8000

ENV UWSGI_WSGI_FILE=information_bot_web/wsgi.py UWSGI_HTTP=:8000 \
UWSGI_MASTER=1 UWSGI_WORKERS=2 UWSGI_THREADS=8 UWSGI_UID=1000 UWSGI_GID=2000 \
UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy

# Start uWSGI
RUN chmod +x docker_config/entrypoint.sh
RUN chmod 777 information_bot_web
RUN chmod 666 information_bot_web/wsgi.py

ENTRYPOINT ["docker_config/entrypoint.sh"]
CMD ["uwsgi", "--http-auto-chunked", "--http-keepalive"]
