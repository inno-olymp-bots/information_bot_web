"""
WSGI config for information_bot_web project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wb_django.settings")

application = get_wsgi_application()

from django.contrib.auth.models import Group, Permission

group_name = "Admin group"
group, created = Group.objects.get_or_create(name=group_name)

if created:
    permissions = ['change_pdffields', 'change_attache', 'change_housing', 'change_meal', 'change_transport',
                   'change_message', 'change_messagecompetitions', 'change_messageregions',
                   'change_messagestatus', 'add_messagestatus', 'change_messageteams', 'change_personalmessage', 'change_question',
                   'change_region', 'change_team', 'add_message', 'change_competition']

    for feat in permissions:
        permission = Permission.objects.get(codename=feat)
        group.permissions.add(permission)

    permission = Permission.objects.get(codename='change_user', content_type_id=12)
    group.permissions.add(permission)
