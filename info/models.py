from django.db import models


class Housing(models.Model):
    class Meta:
        verbose_name = 'Проживание'
        verbose_name_plural = 'Проживание'

    name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='Название',
    )

    info = models.TextField(
        null=True,
        blank=True,
        verbose_name='Информация',
    )

    def __str__(self):
        if self.name is None:
            return "Информация " + str(self.id)
        else:
            return self.name


class Attache(models.Model):
    class Meta:
        verbose_name = 'Атташе'
        verbose_name_plural = 'Атташе'

    name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='Название',
    )

    info = models.TextField(
        null=True,
        blank=True,
        verbose_name='Информация',
    )

    def __str__(self):
        if self.name is None:
            return "Атташе " + str(self.id)
        else:
            return self.name


class Transport(models.Model):
    class Meta:
        verbose_name = 'Транспорт'
        verbose_name_plural = 'Транспорт'

    name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='Название',
    )

    transfer_1 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Трансфер (первый день)',
    )

    shuttle_1 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Шаттл (первый день)',
    )

    transfer_2 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Трансфер (второй день)',
    )

    shuttle_2 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Шаттл (второй день)',
    )

    transfer_3 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Трансфер (третий день)',
    )

    shuttle_3 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Шаттл (третий день)',
    )

    taxi = models.TextField(
        null=True,
        blank=True,
        verbose_name='Такси',
    )

    def __str__(self):
        if self.name is None:
            return "Транспорт " + str(self.id)
        else:
            return self.name


class Meal(models.Model):
    class Meta:
        verbose_name = 'Питание'
        verbose_name_plural = 'Питание'

    name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='Название',
    )

    info_1_day = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание (первый день)',
    )

    info_2_day = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание (второй день)',
    )

    info_3_day = models.TextField(
        null=True,
        blank=True,
        verbose_name='Питание (третий день)',
    )

    def __str__(self):
        if self.name is None:
            return "Питание " + str(self.id)
        else:
            return self.name
