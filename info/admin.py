from django.contrib import admin
from info.models import *
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class HousingResource(resources.ModelResource):
    class Meta:
        model = Housing


class HousingAdmin(ImportExportModelAdmin):
    resource_class = HousingResource


admin.site.register(Housing, HousingAdmin)


class AttacheResource(resources.ModelResource):
    class Meta:
        model = Attache


class AttacheAdmin(ImportExportModelAdmin):
    resource_class = AttacheResource


admin.site.register(Attache, AttacheAdmin)


class TransportResource(resources.ModelResource):
    class Meta:
        model = Transport


class TransportAdmin(ImportExportModelAdmin):
    resource_class = TransportResource


admin.site.register(Transport, TransportAdmin)


class MealResource(resources.ModelResource):
    class Meta:
        model = Meal


class MealAdmin(ImportExportModelAdmin):
    resource_class = MealResource


admin.site.register(Meal, MealAdmin)
