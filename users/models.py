from django.db import models

from messenger.models import MessageRegions
from users import base58
import secrets
from information_bot_web.configs import STATUS_CHOICES, ACTION_CHOICES

MAX_TRIES = 32


class User(models.Model):
    class Meta:
        db_table = "users"
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'

    token = models.CharField(
        max_length=16,
        unique=True,
        null=True
    )

    name = models.CharField(
        max_length=50,
        null=True,
        verbose_name='ФИО'
    )

    alias = models.CharField(
        max_length=50,
        null=True,
        verbose_name='Alias',
        blank=True
    )

    region = models.ForeignKey(
        "users.Region",
        verbose_name='Регион',
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None
    )

    status = models.IntegerField(
        choices=STATUS_CHOICES,
        null=True,
        verbose_name='Статус'
    )

    competition = models.ForeignKey(
        'competitions.Competition',
        verbose_name='Соревнование',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True
    )

    tg_id = models.IntegerField(
        null=True,
        default=None
    )

    state = models.IntegerField(
        default=0
    )

    old_tg_id = models.IntegerField(
        null=True,
        default=None,
    )

    next_user = models.ForeignKey(
        'users.User',
        verbose_name='Копия пользователя',
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True
    )

    def user_action(self, obj):
        pass

    def save(self, *args, **kwargs):
        self.token = self.generate_token()
        super(User, self).save(*args, **kwargs)

    @staticmethod
    def generate_token():

        loop_num = 0
        unique = False
        unique_token = 0
        while not unique:
            if loop_num < MAX_TRIES:
                new_token = str.upper(base58.encode(secrets.token_bytes(9)))
                if not User.objects.filter(token=new_token):
                    unique_token = new_token
                    unique = True
                loop_num += 1
            else:
                raise ValueError("Couldn't generate a unique code.")

        return unique_token

    def __str__(self):
        return "{}".format(self.name)


class Team(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name='номер команды',
    )

    participants = models.ManyToManyField(
        "users.User",
        related_name="teams"
    )

    competition = models.ForeignKey(
        "competitions.Competition",
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None,
        verbose_name='Соревнование',
    )

    def user_action(self, obj):
        pass

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'

    def __str__(self):
        return "{}".format(self.name)


class Region(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='регион'
    )

    code = models.IntegerField(
        verbose_name='код региона',
        default=0,
    )

    housing = models.ForeignKey(
        'info.Housing',
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None,
        blank=True
    )

    transport = models.ForeignKey(
        'info.Transport',
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None,
        blank=True
    )

    attache = models.ForeignKey(
        'info.Attache',
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None,
        blank=True
    )

    def user_action(self, obj):
        pass

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    def __str__(self):
        return "{}".format(self.name)


class JuryGroup(models.Model):
    class Meta:
        verbose_name = 'Чат группа жюри'
        verbose_name_plural = 'Чат группы жюри'

    tg_id = models.CharField(
        max_length=20,
    )

    secret_key = models.CharField(
        max_length=30,
        verbose_name='Секретный ключ',
    )

    def __str__(self):
        return "Группа жюри"


class Statistic(models.Model):
    class Meta:
        verbose_name = 'Статистика'
        verbose_name_plural = 'Статистика'

    tg_id = models.CharField(
        max_length=20,
    )

    status = models.IntegerField(
        choices=STATUS_CHOICES,
        verbose_name='Статус',
        default=7,
    )

    query = models.IntegerField(
        choices=ACTION_CHOICES,
        verbose_name='Действие'
    )

    information = models.CharField(
        max_length=60,
        null=True,
        verbose_name='доп информация'
    )

    datetime = models.DateTimeField(
        verbose_name='Дата и время'
    )

    has_token = models.BooleanField(
        verbose_name='Есть токен?',
        default=False,
    )

    def __str__(self):
        return "Статистика"


class NotRegisteredUsers(models.Model):
    class Meta:
        verbose_name = 'Незарегистрированный пользователь'
        verbose_name_plural = 'Незарегистрированные пользователи'

    tg_id = models.BigIntegerField()
