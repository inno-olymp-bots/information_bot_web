from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.html import format_html
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from information_bot_web.settings import HOST
from messenger.models import *
from .models import *


class UserResource(resources.ModelResource):
    class Meta:
        model = User

    fields = ('ID', 'ID команды', 'Полное имя',)

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        headers = []
        for head in dataset.headers:
            if head == 'ID':
                head = 'id'
            elif head == 'Полное имя':
                head = 'name'
            headers.append(head)
        dataset.headers = headers
        # remove rows REMOVE IT!
        i = 0
        while i < len(dataset):
            if not Team.objects.filter(id=int(dataset[i][1])).exists():
                del dataset[i]
            else:
                i += 1

        col = [0 for _ in range(dataset.height)]  # Participants
        dataset.insert_col(len(dataset.headers), col=col, header="status")

    def before_import_row(self, row, **kwargs):
        team = Team.objects.get(id=int(row['ID команды']))
        team.participants.add(int(row['id']))
        region = team.participants.last().region.id
        row['region'] = region
        team.save()


class UserAdmin(ImportExportModelAdmin):
    list_display = ('name', 'alias', 'status', 'region', 'get_teams', 'competition', 'user_action',)
    readonly_fields = ('alias', 'tg_id', 'token', 'competition', 'state', 'old_tg_id', 'next_user',)
    search_fields = ('name', 'alias', 'tg_id',)
    readonly_fields += ('user_action',)
    list_filter = ('status', 'region',)
    resource_class = UserResource

    def get_teams(self, obj):
        return ', '.join([str(t) for t in obj.teams.all()])

    get_teams.short_description = 'Команды'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<user_id>.+)/sendmessage/$',
                self.admin_site.admin_view(self.process_action),
                name='send-personal-message',
            ),
        ]
        return custom_urls + urls

    def user_action(self, obj):
        return format_html(
            '<a class="button" href="{}"/">Написать</a>',
            reverse('admin:send-personal-message', args=[obj.id])
        )

    def process_action(self, request, user_id):
        user = User.objects.get(id=user_id)
        msg = PersonalMessage(user=user)
        msg.save(send=False)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/personalmessage/{msg.id}/")

    user_action.short_description = 'Сообщение'
    user_action.allow_tags = True


admin.site.register(User, UserAdmin)


class TeamResource(resources.ModelResource):
    class Meta:
        model = Team

    fields = ('Код', 'ID', 'Полное имя', 'Регион', 'Регламент', 'ID тренера')

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        headers = []
        for head in dataset.headers:
            if head == 'Код':
                head = 'name'
            if head == 'ID':
                head = 'id'
            elif head == 'Полное имя':
                head = 'participants'
            elif head == 'Регион':
                head = 'region'
            elif head == 'Регламент':
                head = 'competition'
            headers.append(head)
        dataset.headers = headers

        # remove rows
        i = 0
        while i < len(dataset):
            if str(dataset[i][5]).startswith('Отклонена'):
                del dataset[i]
            else:
                i += 1

    def before_import_row(self, row, **kwargs):
        row['competition'] = int(str(row['competition']).split('.')[0])
        user_name = row['participants']
        user_id = row['ID тренера']
        user_region = Region.objects.get(name=row['region']).id
        user = User.objects.get_or_create(id=user_id, name=user_name,
                                          region_id=user_region, status=1)
        row['participants'] = user[0].id


class TeamConfig(ImportExportModelAdmin):
    list_display = ('name', 'competition', 'get_category', 'get_participants', 'user_action',)
    search_fields = ('name',)
    readonly_fields = ('participants', 'user_action',)
    list_filter = ('competition__category', 'competition',)
    resource_class = TeamResource

    def get_participants(self, obj):
        return ', '.join([str(t) for t in obj.participants.all()])

    def get_category(self, obj):
        return obj.competition.category

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<team_id>.+)/sendmessage/$',
                self.admin_site.admin_view(self.process_action),
                name='send-team-message',
            ),
        ]
        return custom_urls + urls

    def user_action(self, obj):
        return format_html(
            '<a class="button" href="{}"/">Написать</a>',
            reverse('admin:send-team-message', args=[obj.id])
        )

    def process_action(self, request, team_id):
        team = Team.objects.get(id=team_id)
        msg = MessageTeams()
        msg.save(send=False)
        msg.teams.add(team)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messageteams/{msg.id}/")

    def make_action(self, request, queryset):
        msg = MessageTeams()
        msg.save(send=False)
        for team in queryset:
            msg.teams.add(team)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messageteams/{msg.id}/")

    actions = [make_action]
    make_action.short_description = 'Написать сообщение'
    user_action.short_description = 'Сообщение'
    get_category.short_description = 'Категория'
    user_action.allow_tags = True


admin.site.register(Team, TeamConfig)


class RegionResource(resources.ModelResource):
    class Meta:
        model = Region


class RegionConfig(ImportExportModelAdmin):
    list_display = ('name', 'code', 'housing', 'transport', 'attache', 'user_action',)
    search_fields = ('name', 'code',)
    readonly_fields = ('user_action',)
    resource_class = RegionResource

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<region_id>.+)/sendmessage/$',
                self.admin_site.admin_view(self.process_action),
                name='send-region-message',
            ),
        ]
        return custom_urls + urls

    def user_action(self, obj):
        return format_html(
            '<a class="button" href="{}"/">Написать</a>',
            reverse('admin:send-region-message', args=[obj.id])
        )

    def process_action(self, request, region_id):
        region = Region.objects.get(id=region_id)
        msg = MessageRegions()
        msg.save(send=False)
        msg.regions.add(region)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messageregions/{msg.id}/")

    def make_action(self, request, queryset):
        msg = MessageRegions()
        msg.save(send=False)
        for region in queryset:
            msg.regions.add(region)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messageregions/{msg.id}/")

    actions = [make_action]
    make_action.short_description = 'Написать сообщение'
    user_action.short_description = 'Сообщение'
    user_action.allow_tags = True


admin.site.register(Region, RegionConfig)


class JuryGroupConfig(admin.ModelAdmin):
    list_display = ('tg_id', 'secret_key',)
    readonly_fields = ('tg_id',)


admin.site.register(JuryGroup, JuryGroupConfig)


class NotRegisteredUsersConfig(admin.ModelAdmin):
    list_display = ('tg_id',)
    readonly_fields = ('tg_id',)


admin.site.register(NotRegisteredUsers, NotRegisteredUsersConfig)


class StatisticResource(resources.ModelResource):
    class Meta:
        model = Statistic


class StatisticConfig(ImportExportModelAdmin):
    resource_class = StatisticResource
    list_display = ('tg_id', 'status', 'query', 'information', 'datetime', 'has_token')
    readonly_fields = ('tg_id', 'query', 'information', 'datetime', 'status', 'has_token')
    list_filter = ('query', 'information', 'datetime')


admin.site.register(Statistic, StatisticConfig)
